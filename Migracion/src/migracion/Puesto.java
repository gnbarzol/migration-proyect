/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package migracion;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author PC
 */
public class Puesto {
    String id;
    String encargado;
    boolean disponible;
    
    public Puesto(String id, String encargado){
        this.id=id;
        this.encargado=encargado;
        disponible=true;
    }

    public String getId() {
        return id;
    }

    public String getEncargado() {
        return encargado;
    }

    @Override
    public String toString() {
        return "Puesto{" + "id=" + id + ", encargado=" + encargado + ", disponible=" + disponible + '}';
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }
    public static List<Puesto> readFromFile(String file){
        File file1 = new File(file);
       List a = new ArrayList();
       try {

        Scanner sc = new Scanner(file1);

        while (sc.hasNextLine()) {
            String st = sc.nextLine();
            String [] array =st.split(",");                 
            Puesto o = new Puesto(array[0],array[1]);
            a.add(o);
        }
        sc.close();
    } 
    catch (FileNotFoundException e) {
        
        System.out.println("no hay");
    }catch(Exception e){
           System.out.println("otro");
    }
        return a;
    }
    
    public static void main(String[] args){
        List<Puesto> lp = readFromFile("src\\recursos\\archivos\\Puestos.txt");
        System.out.println(lp);
        
    }
    
    
    }
    

