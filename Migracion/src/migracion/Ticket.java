/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package migracion;

/**
 *
 * @author PC
 */
public class Ticket {
    int priority;
    Puesto puesto;
    String turno; 
    
    public Ticket(int priority,Puesto puesto, String turno){
       this.priority=priority;
       this.puesto=puesto;
       this.turno=turno;
    }

    public int getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return "Ticket{" + "priority=" + priority + ", puesto=" + puesto + ", turno=" + turno + '}';
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public String getTurno() {
        return turno;
    }
    
}
