/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author GaryBarzola
 */
public class ViewRegistro {
    private GridPane root;
    
    public ViewRegistro(){
        root = createRegistrationFormPane();
        addUIControls(root);
        root.add(btn_save(), 0, 15, 2, 1);
        root.setStyle("-fx-background-color:white");
    }
    
    private GridPane createRegistrationFormPane() {
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setPadding(new Insets(20, 40, 20, 40));// Set a padding of 40px on each side
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        // Add Column Constraints
        ColumnConstraints columnOneConstraints = new ColumnConstraints(100, 100, Double.MAX_VALUE);// columnOneConstraints will be applied to all the nodes placed in column one.
        columnOneConstraints.setHalignment(HPos.RIGHT);
        
        ColumnConstraints columnTwoConstrains = new ColumnConstraints(200,200, Double.MAX_VALUE);// columnTwoConstraints will be applied to all the nodes placed in column two.
        columnTwoConstrains.setHgrow(Priority.ALWAYS);
        gridPane.getColumnConstraints().addAll(columnOneConstraints, columnTwoConstrains);

        return gridPane;
    }
    
    private void addUIControls(GridPane gridPane) {
        Label headerLabel = new Label("Registro");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0,0,2,1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(0, 0,20,0));

        Label l1 = new Label("Tipo movimiento : ");
        Label l2 = new Label("Viaje transporte : ");
        Label l3 = new Label("Procedencia jefatura migracion: ");
        Label l4 = new Label("Canton jefatura migracion : ");
        Label l5 = new Label("Año movimiento");
        Label l6 = new Label("Mes movimiento");
        Label l7 = new Label("Dia movimiento");
        Label l8 = new Label("Pais de residencia");
        Label l9 = new Label("Motivo de viaje");
        Label l10 = new Label("Pais de precedencia");
        Label l11 = new Label("Lugar de procedencia");
        Label l12 = new Label("Continente procedencia");
        Label l13 = new Label("Continente residencia");
        Label l14 = new Label("Subcontinente procedencia");
        
        gridPane.add(l1, 0,1);
        gridPane.add(l2, 0,2);
        gridPane.add(l3, 0, 3);
        gridPane.add(l4, 0, 4);
        gridPane.add(l5, 0, 5);
        gridPane.add(l6, 0, 6);
        gridPane.add(l7, 0, 7);
        gridPane.add(l8, 0, 8);
        gridPane.add(l9, 0, 9);
        gridPane.add(l10, 0, 10);
        gridPane.add(l11, 0, 11);
        gridPane.add(l12, 0, 12);
        gridPane.add(l13, 0, 13);
        gridPane.add(l14, 0, 14);
        
        crearField(gridPane,14);
    }
    public void crearField(GridPane gP, int n){
        for(int i=1; i<=n; i++){
            TextField balanceField = new TextField();
            balanceField.setPrefHeight(40);
            gP.add(balanceField,1,i);
        }
    }

    
    public final Button btn_save(){
        // Add Done Button
        Button doneButton = new Button("Done");
        VentanaPrincipal.styleButton(doneButton);
        
        // gridPane.add(doneButton, 0, 5, 2, 1);
        GridPane.setHalignment(doneButton, HPos.CENTER);
        GridPane.setMargin(doneButton, new Insets(20, 0,20,0));
        
        doneButton.setOnAction( e -> {
                System.out.println("Holi");
        });
        return doneButton;
    }
    
    public GridPane getRoot(){
        return root;
    }
}
