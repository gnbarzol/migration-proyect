/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author GaryBarzola
 */
public class Test extends Application{
    public static Scene sc;
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        VentanaPrincipal vt= new VentanaPrincipal();
        sc= new Scene(vt.getRoot());
        sc.setFill(Color.TRANSPARENT);
        //primaryStage.initStyle(StageStyle.TRANSPARENT); Me quita todo del stage
        sc.getStylesheets().add("recursos/hojaStilo.css");
        primaryStage.setScene(sc);
        primaryStage.show();
    }
    
    public static void main(String[] args){
        launch(args);
    }
    
    public static Scene getScene() {
        return sc;
    }
    
     
    
}
