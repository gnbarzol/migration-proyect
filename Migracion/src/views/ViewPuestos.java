/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author PC
 */
public class ViewPuestos {
    private BorderPane root;
    private FlowPane puestos;
    ScrollPane sP;

    public ViewPuestos() {
        root = new BorderPane();
        puestos = new FlowPane();
        puestos.setMinWidth(750);
        sP = new ScrollPane();
        root.setTop(titulo());
        sP.setContent(crearEmpleado(20));
        root.setCenter(sP);
        root.setStyle("-fx-background-color:aliceblue");
        puestos.setPadding(new Insets(40,50,30,50));
        
    }
    
    private HBox titulo(){
        HBox top = new HBox();
        Label title = new Label("PUESTOS DEL SISTEMA");
        title.setStyle("-fx-font-weight:bolder; -fx-font-size:26; -fx-text-fill: #ffffff;");
        top.getChildren().add(title);
        top.setAlignment(Pos.CENTER);
        top.setStyle("-fx-background-color: #1883ba");
        return top;
    }
    
    private FlowPane crearEmpleado(int numEmpleado){
        sP.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        sP.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        sP.setPannable(true);
        int count = 0;
        while(count++<numEmpleado){
            VBox contenido = new VBox();
            String puesto = ""+count;
            Label p = new Label("Puesto "+count);
            p.setStyle("-fx-font-weight:bolder; -fx-font-size:20");
            
            Image imgTicket = new Image("/recursos/empleado.png");
            ImageView iv = new ImageView(imgTicket);
            iv.setFitHeight(110);
            iv.setFitWidth(110);
            
            contenido.getChildren().addAll(p, iv);
            contenido.setAlignment(Pos.CENTER);
            contenido.setSpacing(10);
            contenido.setId("contenido");
            contenido.setPadding(new Insets(0,0,30,0));
            
            contenido.setOnMouseClicked((e)->{
                Stage st = new Stage();
                ViewEncargado vE = new ViewEncargado(puesto);
                Scene sc = new Scene(vE.getRoot(),700,550);
                sc.setFill(Color.TRANSPARENT);
                st.setScene(sc);
                st.show();
            });
            
            puestos.getChildren().add(contenido);
        }
        return puestos;
    }
    

    public BorderPane getRoot() {
        return root;
    }
    
    
    
}
