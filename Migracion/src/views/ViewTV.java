/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.util.Iterator;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author GaryBarzola
 */
public class ViewTV {
    Pane root;
    CircularLinkedList<Image> publicidad = new CircularLinkedList<>();
    
    
    public ViewTV(){
        root= new Pane();
        root.setStyle("-fx-background-color:white");
        ImageView logo = new ImageView(new Image("/recursos/opcion1.png"));
        logo.setLayoutX(40);
        logo.setLayoutY(10);
        logo.setFitHeight(120);
        logo.setFitWidth(220);
        Pane publicity= PublicidadPane();
        VBox vistaturnos = CrearVistaTurnos();
        vistaturnos.setLayoutX(500);
        vistaturnos.setLayoutY(30);
        root.getChildren().addAll(publicity,logo,vistaturnos);
        
    }
    
    public Pane PublicidadPane(){        
        Pane publicidadroot = new Pane();                      
        publicidadroot.setLayoutX(20);
        publicidadroot.setLayoutY(145);
        publicidad=crearPublicidad();
        Iterator<Image> it=publicidad.iterator();
        Thread publicityShow = new Thread(()->{
            while(it.hasNext()){
                Image i = it.next();
                ImageView imv= new ImageView(i);
                imv.setFitHeight(340);
                imv.setFitWidth(460);
                Platform.runLater(()->publicidadroot.getChildren().add(imv));
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }Platform.runLater(()->{
                    publicidadroot.getChildren().remove(imv);
                });
            }
            
        });  
        publicityShow.start();        
        return publicidadroot;
        
    }
       
    
    public CircularLinkedList<Image> crearPublicidad(){
        CircularLinkedList<Image> list = new CircularLinkedList<>();
        Image im1= new Image("/recursos/kfcAD.jpg");        
        Image im2 = new Image("/recursos/appleAD.jpg");
        Image im3= new Image("/recursos/cocacola_1.jpg");
        Image im4 = new Image("/recursos/sweetAD.jpg");
        Image im5= new Image("/recursos/depratiAD.jpg");
        list.addLast(im1);
        list.addLast(im2);
        list.addLast(im3);
        list.addLast(im4);
        list.addLast(im5);
        return list;
    }
    
    
    
    public VBox CrearVistaTurnos(){
        VBox contenedor = new VBox();
        ImageView ticket = new ImageView(new Image("/recursos/ticket_1.png"));
        ImageView turno = new ImageView(new Image("/recursos/turno.png"));
        ticket.setFitHeight(63);
        ticket.setFitWidth(153);
        turno.setFitHeight(63);
        turno.setFitWidth(153);        
        HBox cont= new HBox();
        cont.setSpacing(20);
        cont.getChildren().addAll(ticket,turno);
        contenedor.getChildren().add(cont);
        return contenedor;
    }

    public Pane getRoot() {
        return root;
    }
    
    
    

}
