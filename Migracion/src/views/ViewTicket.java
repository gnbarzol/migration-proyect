/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import migracion.Migrante;
import migracion.Puesto;
import migracion.Ticket;
import migracion.Sistema;
import utilities.CONSTANTES;

/**
 *
 * @author GaryBarzola
 */
public class ViewTicket {
    Font theFont = Font.font("Aharoni", FontWeight.BOLD, 40 );
    Pane root;
    ComboBox<Usuario> cbus;
    public enum Usuario {DISCAPACITADO,TERCERA_EDAD,REGULAR}
    int contD;
    int contT;
    int contR;
    ArrayList<Puesto>  puestos;
    
    public ViewTicket(){ 
        Sistema s= new Sistema();
        puestos= s.getPuestos();
        root= new Pane();
        ImageView imv = new ImageView(new Image(CONSTANTES.RUTA_IMGS+"fondoTicket.png"));
        ImageView check = new ImageView(new Image(CONSTANTES.RUTA_IMGS+"check_button.png"));
        check.setFitHeight(75);
        check.setFitWidth(75);
        check.setLayoutX(475);
        check.setLayoutY(225);
        Label titulo = new Label("BIENVENIDO");        
        titulo.setFont(theFont);  
        titulo.setLayoutX(150);
        titulo.setLayoutY(10);
        Label detalles= new Label("Igrese sus datos para obtener un turno");
        detalles.setLayoutX(165);
        detalles.setLayoutY(60);
        VBox dataUsuario= crearDetallesUsuario();
        dataUsuario.setSpacing(10);
        dataUsuario.setLayoutX(20);
        dataUsuario.setLayoutY(90);
        Button b= new Button();
        root.getChildren().addAll(b,imv,titulo, detalles, dataUsuario,check);
        EventHandler<MouseEvent> e = new EventHandler(){
            
            @Override
            public void handle(Event event) {               
                Puesto puesto = ObtenerEdisponible(puestos);
                Usuario u= cbus.getValue();
                Ticket td=null;
                String t = generarTurno(u);               
                switch(u){
                    case DISCAPACITADO:
                        td= new Ticket(1,puesto,t);                                             
                        break;
                    case TERCERA_EDAD:
                        td= new Ticket(2,puesto,t);                        
                        break;
                    case REGULAR:
                        td = new Ticket(3,puesto,t);                     
                        break;
                }
                Font f= cargarFuente();
                root.getChildren().clear();
                VBox v= new VBox();
                Label b= new Label ("Bienvenido, ");
                Label usur =new Label("Usuario.");
                b.setFont(f);
                usur.setFont(f);
               
                HBox htu= new HBox();
                Label ltu= new Label("Su turno es: ");
                Label tu_r = new Label(td.getTurno());
                htu.getChildren().addAll(ltu,tu_r);
                HBox hpu= new HBox();
                Label lpues= new Label("El puesto asignado es: ");
                Label pu_e= new Label(td.getPuesto().getId());  
                hpu.getChildren().addAll(lpues,pu_e);
                v.getChildren().addAll(b,usur,htu,hpu);
                root.getChildren().add(v);
            }
                        
            
        };
        check.setOnMouseClicked(e);
        
    }
    
    public Font cargarFuente(){
        Font f=null;
        try {
                f = Font.loadFont(new FileInputStream(new File("src/recursos/Fuente1.ttf")), 24);                
            } catch (FileNotFoundException ex) {
                System.out.println("error");
            }
        return f;
    }
    
    
    public VBox crearDetallesUsuario(){        
        HBox cedula = new HBox();
        cedula.setSpacing(5);
        Label cd = new Label("Cedula: ");
        TextField txced = new TextField();
        cedula.getChildren().addAll(cd,txced);
        HBox usuario = new HBox();
        usuario.setSpacing(3);
        Label us = new Label("Usuario: ");
        cbus= new ComboBox<>();          
        cbus.setItems(FXCollections.observableArrayList(Usuario.values()));        
        usuario.getChildren().addAll(us,cbus);
        VBox dataUsuario = new VBox();
        dataUsuario.getChildren().addAll(cedula,usuario);        
        return dataUsuario;
    }
    
    public Puesto ObtenerEdisponible(ArrayList<Puesto> puestos){
        for(Puesto p: puestos){
            if(p.isDisponible()) return p;
        }
        return null;
    }
    
    public String generarTurno(Usuario u){
        String s=null;
        contD=0;
        contT=0;
        contR=0;
        switch(u){
                    case DISCAPACITADO:
                        if(contD==99) contD=0;
                        if(contD>10) s= "A"+"0"+contD;   
                        else s= "A"+contD; 
                        contD++;
                        break;
                    case TERCERA_EDAD:
                        if(contT==99) contT=0;
                        if(contT>10) s= "A"+"0"+contT; 
                        else s= "B"+"0"+contT;
                        contT++;
                        break;
                    case REGULAR:
                        if(contR==99) contR=0;
                        if(contR>10) s= "A"+"0"+contR; 
                        else s= "C"+"0"+contR;
                        contR++;
                        break;
                }
        return s;
    }

    public Pane getRoot() {
        return root;
    }
    
}
