/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
/**
 *
 * @author PC
 */
public class CircularLinkedList<E> implements List<E> {
    Node<E> last;    
    int efectivo;
    
    public CircularLinkedList(){
        efectivo=0;
        last=null;
    }
    @Override
    public boolean addFirst(E element) {
        if(element==null) return false;
        Node<E> node = new Node<>(element);
        if (isEmpty()) {
            last=node; 
            last.setNext(node);
        }
        else{  
            node.setNext(last.getNext());    
            last.setNext(node);
            
        }        
    efectivo++;
    return true;
    }

    @Override
    public boolean addLast(E element) {
        if(element==null) return false;
        Node<E> node = new Node<>(element);
        if (isEmpty()) {
            last=node; 
            last.setNext(node);
        }
        else{              
            node.setNext(last.getNext());
            last.setNext(node);
            last=node;
            
            
        }        
    efectivo++;
    return true;
    }
    
    private Node<E> nodeIndex(int index){
        int cont =0;
        Node<E> p = last.getNext();
        while(cont!=index){
            cont++;
            p=p.getNext();
        }
            return p;
    }

    @Override
    public boolean removeFirst() {
        if(isEmpty()) return false;
        if(efectivo==1){
            last.setData(null);
            last= null;
        }else{
            last.getNext().setData(null); //gelp gc
            Node<E> first= last.getNext();          
            last.setNext(first.getNext());
            first=null;
        }
        efectivo--;
        return true;
    }

    @Override
    public boolean removeLast() {
        if(isEmpty()) return false;
        if(efectivo==1){
            last.setData(null);
            last= null;
        }else{
            last.setData(null); //gelp gc
            Node<E> nowlast= nodeIndex(efectivo-2);//O(n) 
            Node<E> exlast =last;
            nowlast.setNext(last.getNext());
            last=nowlast;
            exlast=null;
            
        }
        efectivo--;
        return true;
    }

    @Override
    public boolean isEmpty() {
        return efectivo==0;
    }

    @Override
    public List<E> slicing(int start, int end) {
        List<E> l= new CircularLinkedList();
        if(start<0||end>efectivo||isEmpty()||start>end) return l;         
        Node<E> n_i= nodeIndex(start);
        Node<E> n_f= nodeIndex(end);
        while(n_i!= n_f){
            l.addLast(n_i.getData());
            n_i=n_i.getNext();
        }
        return l;
    }

    @Override
    public E getFirst() {
        if(isEmpty()) return null;
        return last.getNext().getData();
    }

    @Override
    public E getLast() {
        if(isEmpty()) return null;
        return last.getData();
    }

    @Override
    public int size() {
        return efectivo;
    }

    @Override
    public boolean contains(E element) {
        if(element==null || isEmpty()) return false;
        if(last.getData().equals(element)) return true;
        Node<E> p=last.getNext(); 
        do{
           if(p.getData().equals(element)) return true;
           p=p.getNext();
           
        }while(p!=last.getNext());
        
        return false;
    }

    @Override
    public boolean set(int index, E element) {
        if(index<0 || isEmpty()||index>efectivo||element==null) return false;
        
        if(index==0){
            removeFirst();
            addFirst(element);
            
        }
        else if (index==efectivo-1){
            removeLast();
            addLast(element);
            
        }else if(index==efectivo){
            addLast(element);
        }else{
            Node<E> nuevo = new Node<>(element);
            Node<E> prev = nodeIndex(index-1); //O(n)   
            Node<E> nodeErased= nodeIndex(index);//O(n)
            prev.setNext(nuevo);
            nuevo.setNext(nodeErased.getNext());                                           
            nodeErased.setData(null);
            nodeErased= null;            
        }
        return true;        
    
    }

    @Override
    public E get(int index) {
        if(index<0||index>efectivo-1) return null;
        return nodeIndex(index).getData(); //O(n)
    }

    @Override
    public boolean insert(int index, E element) {
        if(index<0 || isEmpty()||index>efectivo||element==null) return false;
        if(index==0) addFirst(element); 
        Node<E> n = new Node(element);    
        Node<E> next= nodeIndex(index);
        Node<E> prev = nodeIndex(index-1);
        prev.setNext(n);
        n.setNext(next);  
        efectivo++;
        return true;   
    }

    @Override
    public boolean remove(int index) {
        if(index<0||index>=efectivo) return false;
        Node<E> ex= nodeIndex(index);
        ex.setData(null);
        Node<E> prev= nodeIndex(index-1);
        prev.setNext(ex.getNext());        
        ex=null;
        efectivo--;
        return true;
    }

    @Override
    public void reverse() {
        if(!isEmpty()){
            Node<E> prev=null;
            Node<E> current= last.getNext();
            last=current;
            Node<E> next = null;            
            while(current != null){
                next = current.getNext();
                current.setNext(prev);
                prev = current;
                current = next;
            }           
        }
    }
    
    public Iterator<E> iterator(){
        //crear una clase anonima para implementar las funciones
        Iterator<E> it = new Iterator<E>() {
            private Node<E> p= last.getNext(); //nodo viajero que empieza en el first
            

            @Override
            //metodo que me dice si donde está el iterador tengo un siguiente para explorar
            public boolean hasNext() {
                return !isEmpty();
            }

            @Override
            //meotodo que me da la data del nodo actual y mover el nodo viajero (igual que scanner);
            public E next() {
                if(isEmpty()) throw new NoSuchElementException();
                E tmp= p.getData();
                p=p.getNext();
                return tmp;
            }
            
        }; 
        return it;
    }

    
    
    @Override
    public boolean equals(Object obj){
        if(!(obj instanceof CircularLinkedList ))return false;
        CircularLinkedList<E> o = (CircularLinkedList<E>)obj;
        if(o.size()!=this.size())return false;
        Node<E> p= last.getNext();
        Node<E> q=o.last.getNext();
        
        do{
            if(!p.getData().equals(q.getData())) return false;
            p=p.getNext();
            q=q.getNext();
        }while(p!=last.getNext());         
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + Objects.hashCode(this.last);
        hash = 61 * hash + this.efectivo;
        return hash;
    }
    
    public String toString(){            
        if(isEmpty()) return "[]";
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Node<E> p=last.getNext(); 
        do{
           sb.append(p.getData().toString()+",");
           p=p.getNext();
           
        }while(p!=last.getNext());        
        return sb.substring(0,sb.length()-1)+"]";
    }
  

}