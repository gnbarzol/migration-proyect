/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author PC
 */
public class VentanaPrincipal{
    private VBox root;
    private Stage stPuestos;
    private ViewPuestos vE;
    
    public VentanaPrincipal(){
        stPuestos = new Stage();
        root = new VBox();
        root.setSpacing(40);
        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(30,175,30,175));
        root.setStyle("-fx-background-color:aliceblue");
        crearMenu();
    }
    
    private void crearMenu(){
        HBox contenedor = new HBox();
        VBox contenedor_1 = new VBox();
        VBox contenedor_2 = new VBox();
        VBox v1 = new VBox();
        VBox v2 = new VBox();
        VBox v3 = new VBox();
        VBox v4 = new VBox();
        
        Label title= new Label("SISTEMA MIGRACIONES");
        title.setStyle("-fx-font-weight:bolder; -fx-font-size:26");
        
        Image imgCrear = new Image("/recursos/+user.png");
        ImageView iv1 = new ImageView(imgCrear);
        iv1.setFitHeight(150);
        iv1.setFitWidth(150);
        
        Image imgTV = new Image("/recursos/tv.png");
        ImageView iv2 = new ImageView(imgTV);
        iv2.setFitHeight(150);
        iv2.setFitWidth(180);
        
        Image imgPuesto = new Image("/recursos/puestos.png");
        ImageView iv3 = new ImageView(imgPuesto);
        iv3.setFitHeight(130);
        iv3.setFitWidth(180);
        
        Image imgTicket = new Image("/recursos/ticket.png");
        ImageView iv4 = new ImageView(imgTicket);
        iv4.setFitHeight(130);
        iv4.setFitWidth(170);
        
        Button crearP = new Button("Crear Puesto"); 
        styleButton(crearP);
        
        Button tv = new Button("Ver Television"); 
        styleButton(tv);
        
        Button puestos = new Button("Seleccionar Puesto"); 
        styleButton(puestos);
        
        Button ticket = new Button("Generar Ticket"); 
        styleButton(ticket);
        
        crearP.setOnMouseClicked((e)->{
            System.out.println("Click boton crear puesto");
        });
        
        tv.setOnMouseClicked((e)->{
            Stage st= new Stage();
            ViewTV vt= new ViewTV();
            Scene sc= new Scene(vt.getRoot(),850,500);
            st.setScene(sc);
            st.show();
            System.out.println("Click boton tv");
        });
        
        ticket.setOnMouseClicked((e)->{
            System.out.println("Click boton ver puestos");
        });
        
        puestos.setOnMouseClicked((e)->{
            vE = new ViewPuestos();
            Scene sc = new Scene(vE.getRoot(),700,550);
            sc.getStylesheets().add("recursos/hojaStilo.css");
            sc.setFill(Color.TRANSPARENT);
            stPuestos.setScene(sc);
            stPuestos.show();
        });
        
        v1.getChildren().addAll(iv1, crearP);
        v1.setSpacing(10);
        v1.setAlignment(Pos.CENTER);
        
        v2.getChildren().addAll(iv2, tv);
        v2.setSpacing(10);
        v2.setAlignment(Pos.CENTER);
        
        v3.getChildren().addAll(iv3, puestos);
        v3.setSpacing(10);
        v3.setAlignment(Pos.CENTER);
        
        v4.getChildren().addAll(iv4, ticket);
        v4.setSpacing(10);
        v4.setAlignment(Pos.CENTER);
        
        contenedor_1.getChildren().addAll(v1, v3);
        contenedor_1.setSpacing(70);
        contenedor_1.setAlignment(Pos.CENTER);
        
        contenedor_2.getChildren().addAll(v2, v4);
        contenedor_2.setSpacing(70);
        contenedor_2.setAlignment(Pos.CENTER);
        
        contenedor.getChildren().addAll(contenedor_1, contenedor_2);
        contenedor.setSpacing(150);
        contenedor.setAlignment(Pos.CENTER);
        
        root.getChildren().addAll(title, contenedor);  
    }

  

    public static void styleButton(Button btn){
        btn.setMinHeight(30);
        btn.setMinWidth(85);
        btn.getStyleClass().add("myButton");
    }
    
    
    public VBox getRoot() {
        return root;
    }
    
    
}
