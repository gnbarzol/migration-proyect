/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author GaryBarzola
 */
public class ViewEncargado {
    private VBox root;
    private String puesto;
    
    public ViewEncargado(String puesto){
        this.puesto = puesto;
        root = new VBox();
        crearMenu();
        
        
    }

    private void crearMenu(){
        
        HBox contenedor = new HBox();
        VBox vRegistrar = new VBox();
        VBox vBuscar = new VBox();
        
        Label l = new Label("Puesto : "+puesto+"   ---  Ticket : S01");
        l.setStyle("-fx-font-weight:bolder; -fx-font-size:26");
        
        Image image1 = new Image("/recursos/registrar.png");
        ImageView iv1 = new ImageView(image1);
        iv1.setFitHeight(150);
        iv1.setFitWidth(150);
        
        Image image2 = new Image("/recursos/buscar.png");
        ImageView iv2 = new ImageView(image2);
        iv2.setFitHeight(150);
        iv2.setFitWidth(150);
        
        Button registrar = new Button("Registrar");
        registrar.setStyle("-fx-background-color:dodgerblue; -fx-text-fill:white; -fx-font-weight:bolder; -fx-font-size:26");
        registrar.setMinHeight(30);
        registrar.setMinWidth(100);
        
        
        Button buscar = new Button("Buscar");
        buscar.setStyle("-fx-background-color:dodgerblue; -fx-text-fill:white; -fx-font-weight:bolder; -fx-font-size:26");
        buscar.setMinHeight(30);
        buscar.setMinWidth(100);
        
        registrar.setOnMouseClicked((e)->{
            Stage st = new Stage();
            ViewRegistro vE = new ViewRegistro();
            Scene sc = new Scene(vE.getRoot(),650,700);
            sc.setFill(Color.TRANSPARENT);
            st.setScene(sc);
            st.show();
        });
        
        buscar.setOnMouseClicked((e)->{
            Stage st = new Stage();
            ViewBusqueda vE = new ViewBusqueda();
            Scene sc = new Scene(vE.getRoot(),650,550);
            sc.setFill(Color.TRANSPARENT);
            st.setScene(sc);
            st.show();
        });
        
        vRegistrar.getChildren().addAll(iv1, registrar);
        vRegistrar.setSpacing(50);
        vRegistrar.setAlignment(Pos.CENTER);
        
        vBuscar.getChildren().addAll(iv2, buscar);
        vBuscar.setSpacing(50);
        vBuscar.setAlignment(Pos.CENTER);
        
        contenedor.getChildren().addAll(vRegistrar,vBuscar);
        contenedor.setSpacing(140);
        contenedor.setAlignment(Pos.CENTER);
        
        root.getChildren().addAll(l,contenedor);
        root.setSpacing(80);
        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(50,200,50,200));
        root.setStyle("-fx-background-color:aliceblue");
        
    }
    
    public VBox getRoot(){
        return root;
    }
    
}
