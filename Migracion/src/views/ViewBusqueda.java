/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author GaryBarzola
 */
public class ViewBusqueda {
    private GridPane root;
    
    public ViewBusqueda(){
        root = createRegistrationFormPane();
        addUIControls(root);
        root.add(btn_search(), 0, 15, 2, 1);
        root.setStyle("-fx-background-color:white");
    }
    
    private GridPane createRegistrationFormPane() {
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setPadding(new Insets(40));// Set a padding of 40px on each side
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        // Add Column Constraints
        ColumnConstraints columnOneConstraints = new ColumnConstraints(105, 105, Double.MAX_VALUE);// columnOneConstraints will be applied to all the nodes placed in column one.
        columnOneConstraints.setHalignment(HPos.RIGHT);
        
        ColumnConstraints columnTwoConstrains = new ColumnConstraints(200,200, Double.MAX_VALUE);// columnTwoConstraints will be applied to all the nodes placed in column two.
        columnTwoConstrains.setHgrow(Priority.ALWAYS);
        gridPane.getColumnConstraints().addAll(columnOneConstraints, columnTwoConstrains);

        return gridPane;
    }
    
    private void addUIControls(GridPane gridPane) {
        Label headerLabel = new Label("Buscar");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0,0,2,1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(0, 0,20,0));

        Label l1 = new Label("Fecha : ");
        Label l2 = new Label("Provincia Origen : ");
        Label l3 = new Label("Canton Origen: ");
        Label l4 = new Label("Lugar de Destino : ");

        gridPane.add(l1, 0,1);
        gridPane.add(l2, 0,2);
        gridPane.add(l3, 0, 3);
        gridPane.add(l4, 0, 4);

        
        TextField t1 = new TextField();
        t1.setPrefHeight(45);
        gridPane.add(t1,1,1);
        
        TextField t2 = new TextField();
        t2.setPrefHeight(45);
        gridPane.add(t2,1,2);
        
        TextField t3 = new TextField();
        t3.setPrefHeight(45);
        gridPane.add(t3,1,3);
        
        TextField t4 = new TextField();
        t4.setPrefHeight(45);
        gridPane.add(t4,1,4);
    }
    
    public final Button btn_search(){
        // Add Done Button
        Button doneButton = new Button("Buscar");
        VentanaPrincipal.styleButton(doneButton);
       // gridPane.add(doneButton, 0, 5, 2, 1);
        GridPane.setHalignment(doneButton, HPos.CENTER);
        GridPane.setMargin(doneButton, new Insets(20, 0,20,0));
        
        doneButton.setOnAction( e -> {
                System.out.println("Holi");
        });
        return doneButton;
    }

    public GridPane getRoot() {
        return root;
    }
    
    
}
